import { StyleSheet } from 'react-native';
import { MKThemeStyles } from 'modules/MKTheme/styles';

export const styles = StyleSheet.create({
  container: {
    padding: MKThemeStyles.smGap,
    width: '100%',
    borderWidth: 1,
    borderColor: 'blue'
  }
});
