export const MKThemeStyles = {
  //colors
  colorBrandPrimary: '#6666ff',
  colorBrandSecondary: '#2f015b',
  colorBrandTertiary: '#a28bb9',
  colorInfoSuccess: '#0ca907',
  colorInfoDanger: '#FF3B30',
  colorInfoWarning: '#faa302',
  colorInfoNew: '#019ada',
  colorContentPrimary: '#6666ff',
  colorContentSecondary: '#ffffff',
  colorTextPrimary: '#ffffff',
  colorTextSecondary: '#454545',
  colorStrokePrimary: '#f9f9f9',
  colorFocusPrimary: '#7dd0f3',

  // gaps
  xsGap: 5,
  smGap: 10,
  mdGap: 15,
  lgGap: 20,
  xlGap: 35,

  // margins
  xsMargin: 5,
  smMargin: 10,
  mdMargin: 15,
  lgMargin: 20,

  // radius
  xsRadius: 5,
  smRadius: 10,
  mdRadius: 15,
  lgRadius: 20,

  //font sizes
  captionFontSize: 10,
  paragraphFontSize: 14,
  labelFontSize: 14,
  iconFontSize: 30,
  h6FontSize: 14,
  h5FontSize: 16,
  h4FontSize: 18,
  h3FontSize: 20,
  h2FontSize: 22,
  h1FontSize: 22,
  typoLinHeight: 22,

  inputHeight: 40,
  inputFontSize: 16,
  inputBorderSize: 1,

  // shadows
  shadowAlpha: {
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 3
  },
  shadowMild: {
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 2
  },
  shadowLow: {
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 1
  },
  shadowDark: {
    shadowColor: 'rgba(0, 0, 0, 0.7)',
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.7,
    shadowRadius: 5,
    elevation: 5
  }
};
