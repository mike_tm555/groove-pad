import MKRow from './Addons/components/MKRow';
import MKCol from './Addons/components/MKCol';

const MKGrid = {
  Row: MKRow,
  Col: MKCol
};

export default MKGrid;
