import MKAwesomeIcon from './Addons/components/MKAwesomeIcon';
import MKCustomIcon from './Addons/components/MKCustomIcon';

const MKIcon = {
  Awesome: MKAwesomeIcon,
  Custom: MKCustomIcon
};

export default MKIcon;
