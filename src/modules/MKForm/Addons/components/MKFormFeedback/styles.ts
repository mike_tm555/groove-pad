import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  danger: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  success: {
    width: '100%'
  },
  warning: {
    width: '100%'
  }
});
