import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  column: {
    width: '100%',
    flexDirection: 'column'
  }
});
