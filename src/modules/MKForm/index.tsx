import MKFormContainer from './Addons/components/MKFormContainer';
import MKFormGroup from './Addons/components/MKFormGroup';
import MKFormField from './Addons/components/MKFormField';
import MKFormFeedback from './Addons/components/MKFormFeedback';
import MKFormLabel from './Addons/components/MKFormLabel';

const MKForm = {
  Container: MKFormContainer,
  Group: MKFormGroup,
  Field: MKFormField,
  Feedback: MKFormFeedback,
  Label: MKFormLabel
};

export default MKForm;
