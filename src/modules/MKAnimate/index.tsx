import MKFadeIn from './Addons/components/MKFadeIn';
import MKRotate from './Addons/components/MKRotate';
import MKScale from './Addons/components/MKScale';
import MKSlide from './Addons/components/MKSlide/Slide';

const MKAnimate = {
  FadeIn: MKFadeIn,
  Rotate: MKRotate,
  Scale: MKScale,
  Slide: MKSlide
};

export default MKAnimate;
