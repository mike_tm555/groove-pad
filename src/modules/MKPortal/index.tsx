import React from 'react';
import MKPortalConsumer from './Addons/components/MKPortalConsumer';
import MKPortalProvider from './Addons/components/MKPortalProvider';

const MKPortal = {
  Provider: MKPortalProvider,
  Consumer: MKPortalConsumer
};

export default MKPortal;
