import colors from './colors';
import form from './form';
import shadows from './shadows';
import layout from './layout';

export default {
  colors,
  form,
  shadows,
  layout,
};
