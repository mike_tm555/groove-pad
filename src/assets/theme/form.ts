import colors from 'assets/theme/colors';

export default {
  field: {
    height: 45,
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
    backgroundColor: colors.content,
    borderColor: colors.border,
    labelColor: colors.label,
    focused: colors.focused,
    lineHeight: 36,
    fontSize: 13,
    fontSizeLarge: 15,
    color: colors.field,
    linkColor: colors.link,
    errorColor: colors.error
  },
  error: {
    borderColor: colors.error,
    color: colors.error
  },
  success: {
    borderColor: colors.success,
    color: colors.success
  },
  icon: {
    size: 22
  }
};
