export default {
  default: {
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 3,
    zIndex: 1
  },
  mild: {
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 2,
    zIndex: 1
  },
  low: {
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 1,
    zIndex: 1
  },
  dark: {
    shadowColor: 'rgba(0, 0, 0, 0.7)',
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.7,
    shadowRadius: 5,
    elevation: 5,
    zIndex: 10
  }
};
