export default {
  appName: 'Miki-Pad',
  effects: 'Effects',
  record: 'Record',
  addPad: 'Add Pad',
  addSound: 'Add new sound Effect',
  screens: {
    main: {
      header: 'Groove Pad'
    },
    records: {
      header: 'Records'
    },
    musics: {
      header: 'Music'
    },
    albums: {
      header: 'Albums'
    },
    artists: {
      header: 'Artists'
    },
    pads: {
      header: 'Recordings'
    },
    libraries: {
      header: 'Libraries'
    },
    settings: {
      header: 'Settings'
    },
    messages: {
      header: 'Messages'
    },
    profile: {
      header: 'Messages'
    },
    categories: {
      header: 'Categories'
    },
    search: {
      header: 'Search'
    }
  }
};
