import { InitialState } from 'store/types/store';

export type PadDto = any;

export type InitialStateDto = InitialState<PadDto[] | null>;
