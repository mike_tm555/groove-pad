import React, { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { Audio } from 'expo-av';
import Assets from 'definitions/assets';
import { SvgIcon } from 'components/SvgIcon';
import styles from './styles';

function Pad(props) {
  const { id, title, icon, sound = null, onPress, loop = true, play = false, style = {}, ...otherProps } = props;

  const [player, setPlayer] = useState(null);
  const [playing, setPlaying] = useState(false);
  const [loading, setLoading] = useState(false);

  const startPlay = async (effect) => {
    await effect.playAsync();
    setPlaying(true);
  };

  const stopPlay = async (effect) => {
    await effect.stopAsync();
    setPlaying(false);
  };

  const onPressPad = async () => {
    if (sound) {
      setLoading(true);

      let soundPlayer = player;

      if (!soundPlayer) {
        soundPlayer = await Audio.Sound.createAsync(sound);
        await soundPlayer.sound.setIsLoopingAsync(loop);
      }

      if (!playing) {
        await startPlay(soundPlayer.sound);
      } else {
        await stopPlay(soundPlayer.sound);
      }

      setPlayer(soundPlayer);
      setLoading(false);
    }

    if (onPress) {
      onPress();
    }
  };

  return (
    <TouchableOpacity style={styles.container} onPress={onPressPad} {...otherProps}>
      {sound && !loading && !playing && (
        <SvgIcon icon={Assets.icons.play} style={styles.icon} fill={styles.icon.color} />
      )}

      {loading && <SvgIcon icon={Assets.icons.spinner} style={styles.icon} fill={styles.icon.color} />}

      {!loading && playing && <SvgIcon icon={Assets.icons.playing} style={styles.icon} fill={styles.icon.color} />}
    </TouchableOpacity>
  );
}

export default Pad;
