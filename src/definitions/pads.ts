import Assets from 'definitions/assets';

export default {
  data: [
    {
      id: -1,
      sound: Assets.soundEffects.dog
    },
    {
      id: -2,
      sound: Assets.soundEffects.alarm
    },
    {
      id: -3,
      sound: Assets.soundEffects.suspense
    },
    {
      id: -4,
      sound: Assets.soundEffects.coin
    },
    {
      id: -5,
      sound: Assets.soundEffects.bleeps
    },
    {
      id: -6,
      sound: Assets.soundEffects.pulse
    },
    {
      id: -7,
      sound: Assets.soundEffects.drum1
    },
    {
      id: -8,
      sound: Assets.soundEffects.drum2
    },
    {
      id: -9,
      sound: Assets.soundEffects.drum3
    },
    {
      id: -10,
      sound: Assets.soundEffects.orchestra
    },
    {
      id: -11,
      sound: Assets.soundEffects.drum3
    },
    {
      id: -12,
      sound: Assets.soundEffects.orchestra
    },
    {
      id: -13,
      sound: Assets.soundEffects.dog
    },
    {
      id: -14,
      sound: Assets.soundEffects.drum1
    },
    {
      id: -15,
      sound: Assets.soundEffects.drum2
    },
    {
      id: -16,
      sound: Assets.soundEffects.drum3
    },
    {
      id: -17,
      sound: Assets.soundEffects.bleeps
    },
    {
      id: -18,
      sound: Assets.soundEffects.pulse
    },
    {
      id: -19,
      sound: Assets.soundEffects.suspense
    },
    {
      id: -20,
      sound: Assets.soundEffects.orchestra
    }
  ]
};
