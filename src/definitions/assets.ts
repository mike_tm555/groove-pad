// patterns
import logo from 'assets/images/patterns/logo';
import icon_libraries from 'assets/images/icons/libraries';
import icon_records from 'assets/images/icons/records';
import icon_musics from 'assets/images/icons/musics';
import icon_albums from 'assets/images/icons/albums';
import icon_artist from 'assets/images/icons/artists';
import icon_pads from 'assets/images/icons/pads';
import icon_settings from 'assets/images/icons/settings';
import icon_notifications from 'assets/images/icons/notifications';
import icon_mixer from 'assets/images/icons/mixer';
import icon_microphone from 'assets/images/icons/microphone';
import icon_create from 'assets/images/icons/create';
import icon_play from 'assets/images/icons/play';
import icon_spinner from 'assets/images/icons/spinner';
import icon_playing from 'assets/images/icons/playing';
import icon_back from 'assets/images/icons/back';
import icon_home from 'assets/images/icons/home';
import icon_messages from 'assets/images/icons/messages';
import icon_search from 'assets/images/icons/search';
import icon_profile from 'assets/images/icons/profile';
import icon_remove from 'assets/images/icons/profile';
//--------

export default {
  patterns: {
    logo,
  },
  languages: {
    enIcon: require('assets/images/languages/en.png'),
    amIcon: require('assets/images/languages/am.png'),
    ruIcon: require('assets/images/languages/ru.png'),
  },
  icons: {
    libraries: icon_libraries,
    musics: icon_musics,
    albums: icon_albums,
    artists: icon_artist,
    pads: icon_pads,
    records: icon_records,
    settings: icon_settings,
    notifications: icon_notifications,
    mixer: icon_mixer,
    microphone: icon_microphone,
    create: icon_create,
    play: icon_play,
    spinner: icon_spinner,
    playing: icon_playing,
    back: icon_back,
    home: icon_home,
    profile: icon_profile,
    search: icon_search,
    messages: icon_messages,
    remove: icon_remove,
  },
  soundEffects: {
    dog: require('assets/sounds/effects/dog.wav'),
    alarm: require('assets/sounds/effects/alarm.wav'),
    suspense: require('assets/sounds/effects/suspense.wav'),
    coin: require('assets/sounds/effects/coin.wav'),
    bleeps: require('assets/sounds/effects/bleeps.wav'),
    pulse: require('assets/sounds/effects/pulse.wav'),
    drum1: require('assets/sounds/effects/drum1.mp3'),
    drum2: require('assets/sounds/effects/drum2.mp3'),
    drum3: require('assets/sounds/effects/drum3.mp3'),
    orchestra: require('assets/sounds/effects/orchestra.wav'),
  },
};
